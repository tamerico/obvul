                 

// ============================================================== 
// Radar chart option
// ============================================================== 
var radarChart = echarts.init(document.getElementById('radar-chart'));

// specify chart configuration item and data

// variaveis criacas pra evitar digitação de nomes errados pois o campo data e campo name devem ter conteudo identico.
dadosReais = 'Dados Reais';
simulacao = 'Simulação';


option = {
    
    tooltip : {
        trigger: 'axis'
    },
    legend: {
        orient : 'vertical',
        x : 'right',
        y : 'bottom',
        data:[dadosReais,simulacao]
    },
    toolbox: {
        show : true,
        feature : {
            dataView : {show: true, readOnly: false},
            restore : {show: true},
            saveAsImage : {show: true}
        }
    },
    polar : [
       {
           indicator : [
               { text: 'Saúde', max: 6000},
               { text: 'InfraEstrutura', max: 16000},
       { text: 'Cultura', max: 30000} /*,
               { text: 'Customer Support', max: 38000},
               { text: 'Development', max: 52000},
               { text: 'Marketing', max: 25000}*/
            ]
        }
    ],
    color: ["#55ce63", "#009efb"],
    calculable : true,
    series : [
        {
            name: 'Real vs Previsão',
            type: 'radar',
            data : [
                {
                    value : [4300, 10000, 28000 /*, 35000, 50000, 19000*/],
                    name : dadosReais  // se mudar esse nome dá erro!
                },
                 {
                    value : [5000, 14000, 20000 /*, 31000, 42000, 21000*/],
                    name : simulacao // se mudar esse nome dá erro!
                }
            ]
        }
    ]
};
                    
                    
                    

// use configuration item and data specified to show chart
radarChart.setOption(option, true), $(function() {
            function resize() {
                setTimeout(function() {
                    radarChart.resize()
                }, 100)
            }
            $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
        });


