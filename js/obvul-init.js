/*
Dependencias para este script, na ordem exibida:
    1 - Jquery
    2 - dadosobvul.js
    3 - maps.js
    4 - radar.js
* Carrega os campos de nomes de RA do campo Select
*
*/
var sel = document.getElementById('inlineFormCustomSelect');
for (var i = 0; i < regionsSelect.length; i++) {
  // console.log("criacao do campo select.  posicao " + i);
  var opt = document.createElement('option');
  opt.innerHTML = regionsSelect[i][0];
  opt.value = regionsSelect[i][1];
  //opt.id = regionsSelect[i];
  sel.appendChild(opt);
}

/*
*  esta funcao é executada no campo select
*  Atualiza todo o dashboard quando o campo select de regiao Administrativa é modificado.
*/
function updateObvul() {

     if (debugObvulInit)
         console.log ("Executando métodod updateObvul()");

  var RA_nome = $("#inlineFormCustomSelect :selected").text();

  var RA_code = $("#inlineFormCustomSelect").val(); // The value of the selected option

  var valorIndiceRA;

  if (RA_code != "selecione") {
            //var x = document.getElementById("inlineFormCustomSelect").selected;

          if (RA_code=="distrito-federal")
          {
            // fazer o mesmo mas o DF tem que ter a mesma estrutura de dadosRA...
            valorIndiceRA = dadosDF["indiceobvul"][0]["distrito-federal"];
          }
          else{

            valorIndiceRA = dadosRA["indiceobvul"][0][RA_code];
          }

              if (debugObvulInit)
                console.log("updateObvul RA:" + RA_code);

            // Atualiza Radar
            atualizaRadar(RA_code);

            // Atualiza o valor do índice dentro do Box Região e Índice
            atualizaNomeRABox(RA_code);
            atualizaIndiceRABox(valorIndiceRA);
            atualizaPosicaoRABox(RA_code);


            // Atualiza mapa
            atualizaMapa(RA_code);


      } else
           if (debugObvulInit)
                console.log("selecione uma RA no campo select.");

      }

/*
*  Funcoes que são executadas somente quando o documento for totalmente carregado
*
*/
$(document).ready(function() {
  //console.log("no ready! ");

    // o codig odentro desta funcao é o mesmo do updateObvul mas enxuto para o DF

    // inicializa com dados do DF
    initDF = "distrito-federal";

    // Atualiza Radar
    atualizaRadar(initDF );

    // Atualiza o valor do índice dentro do Box Região e Índice
    atualizaNomeRABox(initDF );
    valorIndiceRA = dadosDF["indiceobvul"][0]["distrito-federal"];
    atualizaIndiceRABox(valorIndiceRA);
    atualizaPosicaoRABox(initDF );


    // Atualiza mapa
    atualizaMapa(initDF);

    console.log("no ready! 2 ");

});
