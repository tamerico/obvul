/*
Dependencias para este script, na ordem exibida:
    1 - Jquery
    2 - dadosobvul.js
    3 - maps.js
*/

var radarChart = echarts.init(document.getElementById('radar-chart'));

function atualizaRadar(codRA) {


  // variaveis criadas pra evitar digitação de nomes errados pois o campo data e campo name devem ter conteudo identico.
  dadosReais = 'Dados Reais';
  RAcomparativo = 'Simulação';

  if (debugRadar){
      console.log(
        "limites para marcos no radial (estrutural, saude e cultura):"
      );
      console.log(highestEstrutural);
      console.log(highestCultura);
      console.log(highestSaude);

  }


  getNameByNominalCodeRA(codRA);


    var dadosParaRadial;

    if (codRA=="distrito-federal")// este código vem do campo select
    {
        dadosParaRadial = dadosDF;
        if (debugRadar)
          console.log("entrou em dadosdf");
    }
    else{
        dadosParaRadial =  dadosRA;
        if (debugRadar)
          console.log("entrou em dadosra");
    }



    if (debugRadar){
      console.log("atualizaRadar codRA= " + codRA);
      console.log("atualizaRadar regionSelect de <" + codRA + "> = " + getNameByNominalCodeRA(codRA));
      console.log("atualizaRadar dados= " + dadosParaRadial);
      console.log(dadosParaRadial);
    }





  $('#radialRA').html("Gráfico Radial de: " + getNameByNominalCodeRA(codRA));
  option = {

    tooltip: {
      trigger: 'axis'
    },
    legend: {
      orient: 'vertical',
      x: 'right',
      y: 'bottom',
      data: [dadosReais/*,RAcomparativo  <<< necessario adicionar dados abaixo*/]
    },
    toolbox: {
      show: false,
      feature: {
        dataView: {
          show: true,
          readOnly: false
        },
        restore: {
          show: true
        },
        saveAsImage: {
          show: true
        }
      }
    },
    polar: [
      {
        indicator: [
          {
            text: 'Saúde',
            max: 24.7  // atencao o valor NUNCA entre aspas.  Valor definido baseado em http://exame.abril.com.br/brasil/os-melhores-e-os-piores-estados-em-indicadores-de-saude/
          }, {
            text: 'InfraEstrutura',
            max: highestEstrutural //highestEstrutural está em dadosObvul.js
          }, {
            text: 'Cultural',
            max: highestCultura
          }/*,
                                   { text: 'Customer Support', max: 38000},
                                   { text: 'Development', max: 52000},
                                   { text: 'Marketing', max: 25000}*/
        ]
      }
    ],color: [
      "#55ce63", "#009efb"
    ],calculable: true, series: [
      {name: 'Real', type: 'radar', data: [
          {value: [ dadosParaRadial["saude"][0][codRA],dadosParaRadial["estrutural"][0][codRA],dadosParaRadial["cultural"][0][codRA]/*, 35000, 50000, 19000*/
            ],name: dadosReais // se mudar esse nome dá erro!
          }
        ]
      } /* , adicioanar aqui outra serie de dados para comparar com outra RA*/
    ]
  };

  // use configuration item and data specified to show chart
  radarChart. setOption(option, true),$( function() {function
    resize() {setTimeout( function() {radarChart. resize()
      },
      100)
    }
    $(window).on("resize", resize),$(".sidebartoggler").on("click", resize)
  });

}

if (debugRadar)
    console. log("definir em radar.js linha 79 o inicio do grafico radial");

// ao iniciar a pagina, mostra-se dados do plano piloto.
// esta linha é necessária para ativar o radar no site quando embebido dentro do wordpresse do obvul.org
atualizaRadar("distrito-federal");
