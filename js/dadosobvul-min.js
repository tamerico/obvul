var debugRadar = false;
var debugMapa = false;
var debugObvulInit = false;

// variavel para armazenar o nome da regiao anterior, usada nos campos select e mapa.
var regiaoAnterior = "regiaoVaziaInit";
// variavel para inicializar corretamente a limpeza da regiao anterior no mapa.
var initApp = true;

/*
* Esta variavel é utilizada no campo select, no mapa, no radial. É um mapeamento de nome com código
* textual da ra e o codigo numerico da RA.
* A ordem de inclusao de cada RA aqui serve para mostrar em ordem alfabética no campo select ou onde
* mais for necessário.
* O codigo textual foi baseado no codigo das regioes do mapa em maps.js e servira de referencia.
*/
var regionsSelect = [
  // FORMATO : Nome da RA, codigo nominal da RA, codigo da RA
  [
    "Selecione a região", "selecione", 0
  ],
  [
    "Distrito Federal", "distrito-federal", 0
  ],
  [
    "Águas Claras", "aguas-claras", 20
  ],
  [
    "Brazlândia", "brazlandia", 4
  ],
  [
    "Candangolândia", "candangolandia", 19
  ],
  [
    "Ceilândia", "ceilandia", 9
  ],
  [
    "Cruzeiro", "cruzeiro", 11
  ],
  [
    "Fercal", "fercal", 31
  ],
  [
    "Gama", "gama", 2
  ],
  [
    "Guará", "guara", 10
  ],
  [
    "Itapoã", "itapoa", 28
  ],
  [
    "Jardim Botânico", "jardim-botanico", 27
  ],
  [
    "Lago Norte", "lago-norte", 18
  ],
  [
    "Lago Sul", "lago-sul", 16
  ],
  [
    "Núcleo Bandeirante", "nucleo-bandeirante", 8
  ],
  [
    "Paranoá", "paranoa", 7
  ],
  [
    "Park-Way", "park-way", 24
  ],
  [
    "Planaltina", "planaltina", 6
  ],
  [
    "Plano Piloto", "brasilia", 1
  ], // no mapa está definido como brasilia
  [
    "Recanto das emas", "recanto-das-emas", 15
  ],
  [
    "Riacho Fundo 1", "riacho-fundo-1", 17
  ],
  [
    "Riacho Fundo 2", "riacho-fundo-2", 21
  ],
  [
    "Samambaia", "samambaia", 12
  ],
  [
    "Santa Maria", "santa-maria", 13
  ],
  [
    "São Sebastião", "sao-sebastiao", 14
  ],
  [
    "Estrutural (SCIA)", "estrutural", 25
  ], // no mapa está definido como estrutural
  [
    "Sia", "sia", 29
  ],
  [
    "Sobradinho", "sobradinho", 5
  ],
  [
    "Sobradinho 2", "sobradinho-2", 26
  ],
  [
    "Sudoeste/Octogonal", "sudoeste", 22
  ], // no mapa está definido como sudoeste
  [
    "Taguatinga", "taguatinga", 3
  ],
  [
    "Varjão", "varjao", 23
  ],
  ["Vicente Pires", "vicente-pires", 30]
];




/*
* retorna o conteudo Nome dado um codigo nominal de região administrativa
*
*
*/
function getNameByNominalCodeRA(codeRA) {

  for (var k = 0; k < regionsSelect.length; k++) {
    if (regionsSelect[k][1] == codeRA) {
      ///console.log (k + "  *&******** name ==========" + name  + "<<<<<<<<<<<<<<<<<<<encontrado =  "+regionsSelect[k][0]);
      return regionsSelect[k][0];
    }
  }

}

/* codigo gerado dinamicamente */
var dadosRA = { "ranking": [ [ [ "fercal", 0.971 ], [ "jardim-botanico", 0.971 ], [ "sia", 0.97 ], [ "estrutural", 0.97 ], [ "park-way", 0.969 ], [ "nucleo-bandeirante", 0.969 ], [ "candangolandia", 0.968 ], [ "itapoa", 0.968 ], [ "lago-sul", 0.967 ], [ "paranoa", 0.966 ], [ "cruzeiro", 0.966 ], [ "riacho-fundo-2", 0.966 ], [ "vicente-pires", 0.966 ], [ "sobradinho-2", 0.965 ], [ "riacho-fundo-1", 0.965 ], [ "lago-norte", 0.964 ], [ "brazlandia", 0.964 ], [ "planaltina", 0.963 ], [ "sudoeste", 0.962 ], [ "gama", 0.961 ], [ "sobradinho", 0.961 ], [ "guara", 0.96 ], [ "sao-sebastiao", 0.959 ], [ "recanto-das-emas", 0.959 ], [ "varjao", 0.958 ], [ "santa-maria", 0.957 ], [ "aguas-claras", 0.953 ], [ "samambaia", 0.949 ], [ "ceilandia", 0.923 ], [ "taguatinga", 0.902 ], [ "brasilia", 0.735 ] ] ], "indiceobvul": [ { "aguas-claras": 0.953, "brasilia": 0.735, "brazlandia": 0.964, "candangolandia": 0.968, "ceilandia": 0.923, "cruzeiro": 0.966, "fercal": 0.971, "gama": 0.961, "guara": 0.96, "itapoa": 0.968, "jardim-botanico": 0.971, "lago-norte": 0.964, "lago-sul": 0.967, "nucleo-bandeirante": 0.969, "paranoa": 0.966, "park-way": 0.969, "planaltina": 0.963, "recanto-das-emas": 0.959, "riacho-fundo-1": 0.965, "riacho-fundo-2": 0.966, "samambaia": 0.949, "santa-maria": 0.957, "sao-sebastiao": 0.959, "estrutural": 0.97, "sia": 0.97, "sobradinho": 0.961, "sobradinho-2": 0.965, "sudoeste": 0.962, "taguatinga": 0.902, "varjao": 0.958, "vicente-pires": 0.966 } ], "estrutural": [ { "aguas-claras": "27.34", "brasilia": "22.73", "brazlandia": "27.43", "candangolandia": "31.13", "ceilandia": "31.71", "cruzeiro": "29.56", "fercal": "39.54", "gama": "21.31", "guara": "27.92", "itapoa": "30.41", "jardim-botanico": "27.66", "lago-norte": "25.91", "lago-sul": "23.67", "nucleo-bandeirante": "30.82", "paranoa": "34.24", "park-way": "30.69", "planaltina": "30.66", "recanto-das-emas": "41.06", "riacho-fundo-1": "31.21", "riacho-fundo-2": "29.50", "samambaia": "28.16", "santa-maria": "33.99", "sao-sebastiao": "30.77", "estrutural": "34.66", "sia": "24.70", "sobradinho": "28.27", "sobradinho-2": "28.87", "sudoeste": "30.92", "taguatinga": "26.71", "varjao": "28.72", "vicente-pires": "28.77" } ], "saude": [ { "aguas-claras": "11.2", "brasilia": "11.2", "brazlandia": "11.2", "candangolandia": "11.2", "ceilandia": "11.2", "cruzeiro": "11.2", "fercal": "11.2", "gama": "11.2", "guara": "11.2", "itapoa": "11.2", "jardim-botanico": "11.2", "lago-norte": "11.2", "lago-sul": "11.2", "nucleo-bandeirante": "11.2", "paranoa": "11.2", "park-way": "11.2", "planaltina": "11.2", "recanto-das-emas": "11.2", "riacho-fundo-1": "11.2", "riacho-fundo-2": "11.2", "samambaia": "11.2", "santa-maria": "11.2", "sao-sebastiao": "11.2", "estrutural": "11.2", "sia": "11.2", "sobradinho": "11.2", "sobradinho-2": "11.2", "sudoeste": "11.2", "taguatinga": "11.2", "varjao": "11.2", "vicente-pires": "11.2" } ], "cultural": [ { "aguas-claras": 152.87, "brasilia": 0, "brazlandia": 213.1, "candangolandia": 237.45, "ceilandia": 73.34, "cruzeiro": 225.37, "fercal": 264.2, "gama": 199.45999999999998, "guara": 185.68, "itapoa": 241.25, "jardim-botanico": 267.07, "lago-norte": 210.64999999999998, "lago-sul": 234.31, "nucleo-bandeirante": 250.51, "paranoa": 220.51999999999998, "park-way": 244.82, "planaltina": 201.07999999999998, "recanto-das-emas": 169.7, "riacho-fundo-1": 216.35, "riacho-fundo-2": 223.07999999999998, "samambaia": 138.06, "santa-maria": 163.34, "sao-sebastiao": 176.70999999999998, "estrutural": 252.13, "sia": 260.01, "sobradinho": 191.52999999999997, "sobradinho-2": 218.74, "sudoeste": 195.88, "taguatinga": 53.84, "varjao": 172.01, "vicente-pires": 226.23 } ] };


/* codigo gerado dinamicamente */
var dadosDF = { "indiceobvul": [ { "distrito-federal": "0.953" } ], "estrutural": [ { "distrito-federal": "29.646" } ], "saude": [ { "distrito-federal": "11.200" } ], "cultural": [ { "distrito-federal": "196.106" } ] };



/* codigo gerado dinamicamente */
var radialLimites = [{"highestEstrutural":41.056301349432495},{"highestCultura":295.25922291439804},{"highestSaude":100}];


// no ordem que esta no array
  console.log (radialLimites[0].highestEstrutural);
  console.log (radialLimites[1].highestCultura);
  console.log (radialLimites[2].highestSaude);

  highestEstrutural = radialLimites[0].highestEstrutural;
  highestCultura = radialLimites[1].highestCultura;
  highestSaude = radialLimites[2].highestSaude;



  /*
  * retorna o codigo nominal da região administrativa dado o codigo numerico da ra estabelecido em lei.
  *
  *
  */
  function getNominalCodeByCodeRA_lei(codeRA_lei) {

    for (var k = 0; k < regionsSelect.length; k++) {
      if (regionsSelect[k][2] == codeRA_lei) {
        //console.log ("getNominalCodeByCodeRA_lei(codeRA_lei) [" + k + "]  "+ regionsSelect[k][2] + " retorna: "+ regionsSelect[k][1] );
        return regionsSelect[k][1];
      }
    }

  }

  /*
  verificar pois esta funcao nao esta sendo usada neste javascript mas pode estar sendo referenciada em outro
  */
  function findElement(arr, propName, propValue) {
    for (var i = 0; i < arr.length; i++)
      if (arr[i][propName] == propValue)
        return arr[i];

        // will return undefined if not found; you could return a default instead
    }



/*
* Função para atualizar o box Nome RA
*
*/
function atualizaNomeRABox(RA_code) {
  $('#nomeregiao').html(" " + RA_code.toUpperCase() + " &nbsp; ");
}

/*
* Função para atualizar o valor do indice da RA no box
*
*/

function atualizaIndiceRABox(valorIndiceRA) {
  $('span.indice').html(" " + valorIndiceRA + " &nbsp;");
}

/*
* Função para atualizar o valor do ranking da RA no box
*
*/
function atualizaPosicaoRABox(nomeCodificadoRA) {
  // olha no dadosRA ranking qual a posicao da RA
  // get index array by name
  var indice =0;
  for(var i = 0; i < dadosRA["ranking"][0].length; i++) {
      //console.log("i :" + dadosRA["ranking"][0][i][0]);
      if (nomeCodificadoRA!="distrito-federal")
      {
         if(dadosRA["ranking"][0][i][0] === nomeCodificadoRA) {
           indice = i+1;
         }
       }
       else indice = "";
  }

  //console.log ("indice: " + indice  );
  $('span.posicao').html(" " + indice);


}


function getMarcoInfraestruturaByNomeRA(nomeCodificadoRA) {
  //console.log("getMarcoInfraestruturaByNomeRA de " +nomeCodificadoRA + ": "+  dadosRA["estrutural"][0][nomeCodificadoRA]);
  return   Number(dadosRA["estrutural"][0][nomeCodificadoRA]).toFixed(3);
}

function getMarcoSaudeByNomeRA(nomeCodificadoRA) {
  //console.log("getMarcoSaudeByNomeRA de " +nomeCodificadoRA + ": "+  dadosRA["saude"][0][nomeCodificadoRA]);
  return   Number(dadosRA["saude"][0][nomeCodificadoRA]).toFixed(3);
}

function getMarcoCulturalByNomeRA(nomeCodificadoRA) {
  //console.log("getMarcoCulturalByNomeRA de " +nomeCodificadoRA + ": "+  dadosRA["cultural"][0][nomeCodificadoRA]);
  return   Number(dadosRA["cultural"][0][nomeCodificadoRA]).toFixed(3);
}

function getValorIndiceObvulByNomeRA(nomeCodificadoRA) {
  //console.log("getValorIndiceObvulByNomeRA de " +nomeCodificadoRA + ": "+  dadosRA["indiceobvul"][0][nomeCodificadoRA]);
  return   Number(dadosRA["indiceobvul"][0][nomeCodificadoRA]).toFixed(3);
}


function getRanking() {
  console.log("getRanking  ");
  console.log(   dadosRA["ranking"][0]   );
  return    dadosRA["ranking"][0];
}



//console.log("dadosInfraPDAD json:");
//console.log(dadosInfraPDAD);

console.log("DADOS RA: ");
console.log(dadosRA);
